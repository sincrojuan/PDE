/*Declare una estructura para almacenar datos de estudiantes (DNI y dos notas correspondientes
  a los doscuatrimestres del año). Hagaun programa que permita ingresar los datos de 5 estudiantes en unvector de
  estas estructuras. Luego de ingresar los datos se deben mostrarlos DNI de cada estudiante y el promedio que tiene en sus examenes.
*/

#include<stdio.h>
int i=0;

struct ALUMNOS
{
    int DNI;
    float cuat1;
    float cuat2;
} alum[5];


main(){

for (i=0;i<5;i++){                                //for carga alumnos
    printf("Ingresedatos del alumno N%d:", i+1);
    printf("\nDNI:");
    scanf("%d", &alum[i].DNI);
    printf("\nNota 1er cuatri:");
    scanf("%f", &alum[i].cuat1);
    printf("\nNota 2do cuatri:");
    scanf("%f", &alum[i].cuat2);
}

for (i=0;i<5;i++){                                //for promedios
    printf("\nAlumno %d: ", i+1);
    printf("\nDNI: %d", alum[i].DNI);
    printf("\tPromedio: %.1f\n", (alum[i].cuat1 + alum[i].cuat2)/2);
}

}
