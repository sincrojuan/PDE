/*Una aerolínea tiene vuelos, los cuales poseen un código alfanumérico (ej: AR1500), una ciudad de orígen y una ciudad destino
  Ingrese 4 vuelos en un vector. Luego de ingresados los datos permita que el usuario escriba el nombre de una ciudad y muestre
  los vuelos que parten y los que arriban a esa ciudad.
  Si no hubiera vuelos para la ciudad ingresada debe mostrarse un mensaje de error.
*/

#include<stdio.h>
int i=0;
char CIUDAD[25];

struct VUELOS
{
    char CODE[7];
    char ORIG[25];
    char DEST[25];
} Vuelo[4];


main(){

for (i=0;i<4;i++){                              //for carga de vuelos
    printf("Ingrese datos del vuelo N%d:", i+1);
    printf("\nCodigo:");
    scanf("%s", &Vuelo[i].CODE);
    printf("\nOrigen:");
    scanf("%s", &Vuelo[i].ORIG);
    printf("\nDestino:");
    scanf("%s", &Vuelo[i].DEST);
}

printf("\n\nSe buscan vuelos para ");               //Búsqueda
scanf("%s", &CIUDAD);

printf("\nVuelos con origen coincidente:");
for (i=0;i<4;i++){                                 //Comparación Origenes
    if(!strcmp(Vuelo[i].ORIG, CIUDAD)){
        printf("\t%s", Vuelo[i].CODE);
    }
}

printf("\n\nVuelos con destino coincidente:");
for (i=0;i<4;i++){                                 //Comparación Destinos
    if(!strcmp(Vuelo[i].DEST, CIUDAD)){
        printf("\t%s", Vuelo[i].CODE);
    }
}

}
