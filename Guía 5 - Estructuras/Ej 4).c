/*Una ferretería tiene un listado de facturas emitidas en cierto año, con estos datos:
número de factura, fecha de emision(dia y mes), nombre del cliente y monto total.
Dado un vector de 10 facturas, se necesitan mostrar en pantalla cuál fue
el mejor mes (o sea el de mayor dinero facturado). Los dato se pueden ingresar por
teclado o dejarlos fijos en el programa para no perder tiempo en tipear datos.
*/

#include<stdio.h>
int i, mayor=0;
float MESES[12]={0,0,0,0,0,0,0,0,0,0,0,0};

typedef struct FECH
{
    int DIA;
    int MES;
}fecha;
struct FACTURAS
{
    int NUMERO;
    fecha FECHA;
    char CLIENTE[25];
    float MONTO;
} FAC[10];


main(){
    
for(i=0;i<10;i++)FAC[i].NUMERO = i+1;          //Numeración automática de facturas

for(i=0;i<10;i++){                             //Carga de datos de factura
    printf("\nIngrese fecha:\nDia:\t");
    scanf("%d", &FAC[i].FECHA.DIA);
    printf("Mes:\t");
    scanf("%d", &FAC[i].FECHA.MES);
    printf("Ingrese cliente:\n");
    scanf("%s", &FAC[i].CLIENTE);
    printf("Ingrese monto:\n");
    scanf("%f", &FAC[i].MONTO);
}

for(i=0;i<10;i++){                            //Acumula lo facturado en cada mes
     MESES[FAC[i].FECHA.MES-1]=MESES[FAC[i].FECHA.MES-1]+FAC[i].MONTO;
}

for(i=1;i<12;i++){                          //Calcula el mejor mes
    if(MESES[i]>=MESES[mayor])
        mayor=i;
}
printf("\nEl mes %d fue el m%cs rentable", mayor+1, 160);

}
