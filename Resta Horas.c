//Juan Bongiorno, Alvaro Milman y Tomás Brancatisano

#include <stdio.h>

typedef struct TIEMPO
{
    int HS;
    int MINS;
    int SEGS;
}HORARIO;
HORARIO h1, h2, resul;

HORARIO DIFERENCIA_HS(HORARIO h1,HORARIO h2);

main(){
printf("Ingrese primer horario\n");
printf("Hora:\t");
scanf("%d", &h1.HS);
printf("\nMinutos:\t");
scanf("%d", &h1.MINS);
printf("\nSegundos:\t");
scanf("%d", &h1.SEGS);

printf("Ingrese segundo horario\n");
printf("Hora:\t");
scanf("%d", &h2.HS);
printf("\nMinutos:\t");
scanf("%d", &h2.MINS);
printf("\nSegundos:\t");
scanf("%d", &h2.SEGS);

//printf("\n\n%d:%d:%d\t%d:%d:%d", h1.HS, h1.MINS, h1.SEGS, h2.HS, h2.MINS, h2.SEGS);

resul = DIFERENCIA_HS(h1, h2);

printf("\nResultado: %d:%d:%d", resul.HS, resul.MINS, resul.SEGS);
}
HORARIO DIFERENCIA_HS(HORARIO h1, HORARIO h2){
    resul.HS=h1.HS - h2.HS;
    resul.MINS=h1.MINS - h2.MINS;
    resul.SEGS= h1.SEGS - h2.SEGS;

    if(resul.SEGS<0){
        resul.SEGS= 60+resul.SEGS;
        resul.MINS--;
    }
    if(resul.MINS<0){
        resul.MINS= 60+resul.MINS;
        resul.HS--;
    }
return(resul);
}
