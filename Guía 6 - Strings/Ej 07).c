/******************************************************************************
7) El usuario ingresará 5 nombres de personas y sus edades (número entero). 
Luego de finalizar el ingreso, muestre en pantalla el nombre de la persona más joven.
El ingreso se realiza de este modo: nombre y edad de la primera persona, luego nombre y edad de la segunda, etc...
Nota: no hay que almacenar todos los nombres y todas las notas.
*******************************************************************************/
#include <stdio.h>

char nombre[21], ingresado[21];
int edad=15000, numero, i=0;

int main()
{
    for(i=0;i<5;i++){
        printf("Ingrese nombre\t");
        scanf("%s", &ingresado);
        
        printf("Ingrese edad\t");
        scanf("%d", &numero);
        if(numero < edad){                    //compara la edad ingresada con la almacenada
            edad=numero;                      //si es menor la guarda
            strcpy(nombre, ingresado);        //y guarda el nombre
        }
    } //se repite 5 veces
    
    printf("\nLa persona m%cs j%cven es %s", 160, 162, nombre);

return 0;
}
