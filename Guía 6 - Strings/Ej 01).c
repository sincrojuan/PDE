/******************************************************************************
1) Permitir que el usuario ingrese una palabra de hasta 20 letras. Mostrar en pantalla cuántas letras tiene la palabra ingresada.
Por ejemplo "CASA" debe indicar 4 letras.
*******************************************************************************/
#include <stdio.h>

int main()
{
    int i=0;
    char palabra[21];
    
    printf("\nIngrese una palabra:\n");      //ingreso de la palabra
    scanf("%s", &palabra);
    for(i=0;i<22;i++){
        if(palabra[i]=='\0'){                //cuando llega al caracter \0 deja de contar
        if(palabra[i] == 'Y') printf("B");
        if(palabra[i] == 'Z') printf("C");
            break;
        }
    }
    printf("Tiene %d letras", i);            //imprime donde quedo
    
return 0;
}
