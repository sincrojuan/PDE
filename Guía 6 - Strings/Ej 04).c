/******************************************************************************
4) El usuario ingresa una palabra. Mostrar en pantalla cuántas letras A minúsculas contiene.
*******************************************************************************/
#include <stdio.h>

int main()
{
    int i=0, cont=0;
    char palabra[21];
    
    printf("\nIngrese una palabra:\n");
    scanf("%s", &palabra);
    
    for(i=0;i<22;i++){
        if(palabra[i]=='a')cont++;          //recorre el vector sumando al contador por cada 'a'
    }
    
    printf("\nContiene %d A min%csculas", cont, 163);       //y lo imprime
  
return 0;
}
