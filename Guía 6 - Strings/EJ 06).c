/*6) El usuario ingresa una palabra. Determinar qué letra aparece mayor cantidad de veces.
Para simplificar el problema, trabaje solo con mayúsculas.
*/
#include <stdio.h>

int main()
{
    char palabra[21], abecedario[28]="ABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
    int i, j, cont=0, Aux=0, letra;

    printf("\nIngrese una palabra(en may%csculas)\n", 163);
    scanf("%s", &palabra);

    for(i=0;i<28;i++){
        for(j=0;j<20;j++){
            if(abecedario[i]==palabra[j])     //recorre el vector comparandolo con cada letra del abecedario
                cont++;
        }
        if(cont > Aux){               //para cada letra compara si se repite mas veces que la antes guardad
            Aux = cont;
            letra = i;
            cont = 0;
            printf("Aux=%d  letra=%d\n", Aux, letra);
        }
    }

    if(Aux==1)
        printf("Ninguna letra se repite");               //imprime segun lo contado
    else{
    printf("La que m%cs se repite es %c", 160, abecedario[letra]);
    }

    return 0;
}
