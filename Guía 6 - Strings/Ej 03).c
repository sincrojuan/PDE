/******************************************************************************
3) El usuario ingresa dos strings. Mostrar en pantalla si son iguales o no, 
es decir, si tienen las mismas letras en las mismas posiciones.
*******************************************************************************/
#include <stdio.h>

int main()
{
    int i=0, flg=0;
    char palabra1[21]="00000000000000000000", palabra2[21]="00000000000000000000";
    
    printf("\nIngrese la primer palabra:\n");
    scanf("%s", &palabra1);
    
    printf("Ingrese la segunda palabra:\n");
    scanf("%s", &palabra2);
    
    for(i=0;i<22;i++){
        if(palabra1[i]!=palabra2[i]){        //va comparando letras de ambas palabras
            flg=1;
            break;                              //si son distintas flg=1 y deja de recorrer
        }
    }
    
    if(flg==0)
        printf("Las palabras son iguales");         //segun la flag imprime
    if(flg==1)
        printf("Las palabras son diferentes");    
return 0;
}
