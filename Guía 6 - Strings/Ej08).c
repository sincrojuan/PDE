/******************************************************************************
8) El usuario ingresará nombres de personas hasta que ingrese la palabra "FIN". No sabemos cuántos nombres ingresará.
Luego de finalizar el ingreso, mostrar en pantalla cuál es el primer nombre en orden alfabético de todos los ingresados.
*******************************************************************************/
#include <stdio.h>

int main()
{
    char nombre[21]="ÑÑ", primero[21]="ÑÑ";

    printf("\nIngrese nombre(s) o FIN para terminar\n");

    while(strcmp(nombre, "FIN")){ //mientras el nombre ingresado sea diferente a FIN
        if(strcmp(nombre, primero) < 0){ //strcmp devuelve un valor negativo si el primer caracter 
            strcpy(primero, nombre);      //que compara esta antes en la tabla ascii
        }                                    //si es negativo lo guarda

        scanf("%s", &nombre);
    }

    printf("\nEl primer nombre alfab%cticamente es: %s", 130, primero);

return 0;
}
