/******************************************************************************
5) El usuario ingresa una palabra. 
Mostrar en pantalla cuántas vocales minúsculas y mayúsculas contiene.
*******************************************************************************/
#include <stdio.h>

int main()
{
    int i=0, contm=0, contM=0;
    char palabra[21];
    
    printf("\nIngrese una palabra:\n");
    scanf("%s", &palabra);
    
    for(i=0;i<21;i++){                              //si tiene alguna vocal minuscula suma a un contador
        if(palabra[i]=='a' || palabra[i]=='e' || palabra[i]=='i' ||
             palabra[i]=='o' || palabra[i]=='u')
           contm++;
        if(palabra[i]=='A' || palabra[i]=='E' || palabra[i]=='I' ||
            palabra[i]=='O' || palabra[i]=='U')
           contM++;                                 //si es mayuscula suma en otro contador
    }
    
    printf("\nContiene %d vocles min%csculas", contm, 163);
    printf("\nContiene %d vocles may%csculas", contM, 163);
  
return 0;
}
