/******************************************************************************
2) Permitir el ingreso de una palabra y mostrarla en pantalla al revés.
Por ejemplo, para "CASA" se debe mostrar "ASAC".
*******************************************************************************/
#include <stdio.h>

int main()
{
    int i=0;
    char palabra[21]="000000000000000000000";
    
    printf("\nIngrese una palabra:\n");
    scanf("%s", &palabra);
    for(i=20;i>-1;i--){                    //lee el vector al reves
        if(palabra[i]!='0')
            printf("%c", palabra[i]);             //y lo va imprimiendo
        }
    
return 0;
}
