//9)b) Al ejercicio anterior agrgue que se muestre cuál equipo es el ganador (A ó B). 
//El ganador final es el que más partidos ha ganado. Considere que pueden tener igual 
//cantidad de partidos ganados, en cuyo caso se dictamina un empate.
#include <stdio.h>

int i, winsA=0, winsB=0, empates=0, equipoA[6], equipoB[6];

int main(){
    printf("\nIngrese goles de A");
    for(i=0;i<6;i++){
        printf("\nPartido %d: ", i+1);
        scanf("%d", &equipoA[i]);
    }
    printf("\nIngrese goles de B");
    for(i=0;i<6;i++){
        printf("\nPartido %d: ", i+1);
        scanf("%d", &equipoB[i]);
    }
    
    for(i=0;i<6;i++){
        if(equipoA[i]==equipoB[i])empates++;
        if(equipoA[i]<equipoB[i])winsB++;
        if(equipoA[i]>equipoB[i])winsA++;
    }
    printf("\nHubo %d empates", empates);
    if(winsA>winsB)printf("\nGan%c el equipo A", 162);
    if(winsA<winsB)printf("\nGan%c el equipo B", 162);
    if(winsA==winsB)printf("\nSe dictamina un empate");

return 0;
}

