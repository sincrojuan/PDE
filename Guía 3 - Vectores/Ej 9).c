//9) En dos vectores se tienen los goles de dos equipos en diferentes partidos que 
//tuvieron entre si. En la posicion 0 del vector equipoA están los goles de un partido por
//ese equipo y en la misma posición del vector equipoB están los goles del otro equipo 
//en ese mismo partido. En la posición 1 de ambos vectores tendremos los goles para otro 
//partido, etc. Se pide permitir que el usuario cargue los datos y mostrar cuántos empates 
//han ocurrido
#include <stdio.h>

int i, cont, equipoA[5], equipoB[5];

int main(){
    printf("\nIngrese goles de A");
    for(i=0;i<5;i++){
        printf("\nPartido %d: ", i+1);
        scanf("%d", &equipoA[i]);
    }
    printf("\nIngrese goles de B");
    for(i=0;i<5;i++){
        printf("\nPartido %d: ", i+1);
        scanf("%d", &equipoB[i]);
    }
    
    for(i=0;i<5;i++){
        if(equipoA[i]==equipoB[i])cont++;
    }
    printf("\nHubo %d empates", cont);

return 0;
}

