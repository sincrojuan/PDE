//12) Dados los vectores del ejercicio anterior, imprima todas las cobinaciones. 
//Por ejemplo: 1ro de A con 1ro de B, 1ro de A con 2do de B, 1ro de A con 3ro de B...
//2do de A con 1ro de B, 2do de A con 2do de B... etc.
#include <stdio.h>

int i, j, A[3]={1,6,29}, B[10]={0,1,2,3,4,5,6,7,8,9};

int main(){
    for(i=0;i<3;i++){
        for(j=0;j<10;j++){
            printf("%d-%d\n", A[i], B[j]);
            }
    }

return 0;
}

