//14) Dado un vector de 8 floats, imprima en pantalla si todos sus elementos son 
//idénticos o no

#include <stdio.h>
int i, flg=0;
float A[8]={1.1, 2.1, 3.1, 4.1, 5.1, 6.1, 7.1, -1.1}; //CASO DISTINTOS
//float A[8]={1.1,1.1,1.1,1.1,1.1,1.1,1.1,1.1}; //CASO IDENTICOS


int main(){
    for(i=0;i<7;i++){
        if(A[i]==A[i+1])flg++;
        else{break;}
    }
    if(flg==7)printf("Todos los elementos del vector son id%cnticos\n", 130);
    else{
        printf("El vector tiene elementos distintos\n");
    }

return 0;
}
