//10) Se cuenta con dos vectores de 8 enteros cada uno. Estos vectores tienen DNIs de 
//jugadores de tenis. Se pide ingresar los datos y mostrar en pantalla las parejas,
// armadas así: el primero de un vector con el último del otro, el segundo con el anteúltimo, etc.
#include <stdio.h>

int i, j, dnis1[8], dnis2[8];

int main(){
    printf("\nIngrese el primer grupo");
    for(i=0;i<8;i++){
        printf("\nJugador %d: ", i+1);
        scanf("%d", &dnis1[i]);
    }
    printf("\nIngrese el segundo grupo");
    for(i=0;i<8;i++){
        printf("\nJugador %d: ", i+1);
        scanf("%d", &dnis2[i]);
    }
    printf("\nLas parejas finales son: ");
    for(i=0,j=7;i<8 && j>-1;i++,j--){
        printf("\n%d\tcon\t%d", dnis1[i], dnis2[j]);
    }

return 0;
}

