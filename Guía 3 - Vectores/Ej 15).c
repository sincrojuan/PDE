//15) Dado un vector de 10 enteros, imprima sus valores en pantalla sin repetirlos. 
//Es decir que si un valor aparece más de una vez solo hay que imprimirlo una.
#include <stdio.h>

int i, j, flg=0, k=0, aux[5]={0,0,0,0,0}, A[10]={1,2,3,4,1,5,6,2,7,1};

int main(){
    for(i=0;i<10;i++){
        for(j=0;j<10;j++){
            if(i!=j && A[i]==A[j]){
                if(A[i]==0 || (A[i]!=aux[0] && A[i]!=aux[1] && A[i]!=aux[2] && A[i]!=aux[3] && A[i]!=aux[4] && A[i]!=aux[5])){
                    aux[k]=A[i];
                    k++;
                    flg=1;
                    break;
                }
                else{
                    flg=1;
                    break;
                }
            }
        }
        if(flg==1)flg=0;
        else{printf("%d\t", A[i]);}
    }
    printf("\nLos que se repiten son: ");
    for(i=0;i<k;i++){
        printf("%d\t", aux[i]);
    }

return 0;
}
