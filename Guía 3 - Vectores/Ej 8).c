//8) Declare un vector de 10 enteros y carguelo con 1 y 0 alternativamente. 
//Es decir, en la primer posición un 1, en la segnda un 0, en latercera un 1, en la 
//cuarta un 0... etc. Muestrelo en pantalla para verificar que quedó bien cargado.
#include <stdio.h>

int i, A[10]={1,0,1,0,1,0,1,0,1,0};

int main(){
    for(i=0;i<10;i++){
    printf("\nVector[%d] = %d", i, A[i]);
    }
    return 0;
}

